var gulp = require('gulp');
var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var tslint = require('gulp-tslint');
var mocha = require('gulp-mocha');
var webpack = require('webpack');
var webpackConfig = require('./webpack.config.js');

gulp.task('tslint-server', () => {

    var tsProject = typescript.createProject('server/tsconfig.json');

    tsProject.src()
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report());
});

gulp.task('tslint-client', () => {

    var tsProject = typescript.createProject('client/tsconfig.json');

    tsProject.src()
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report());
});

gulp.task('build-server', () => {
    var tsProject = typescript.createProject('server/tsconfig.json');

    var tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject());

    return tsResult.js
        .pipe(sourcemaps.write('.', { sourceRoot: '../../../src/server' }))
        .pipe(gulp.dest('build/server'));
});

gulp.task('build-client', () => {
    var tsProject = typescript.createProject('client/tsconfig.json');

    var tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject());

    return tsResult.js
        .pipe(sourcemaps.write('.', { sourceRoot: '../../../src/server' }))
        .pipe(gulp.dest('build/client'));
});

gulp.task('pack-client', (callback) => {
    webpack(webpackConfig, callback);
});

gulp.task('copy-client', () => {
    return gulp.src(['client/src/**/*.html'])
        .pipe(gulp.dest('build/client/dist'));
});

gulp.task('build-all', ['tslint-server', 'build-server']);

gulp.task('test', ['build-all'], () => {
    gulp.src('build/server/test/**/*.spec.js')
        .pipe(mocha());
});
