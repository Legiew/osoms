module.exports = {
    context: __dirname + '/build/client/src',
    entry: {
        bundle: './app/main.js',
        poly: './poly.js'
    },
    output: {
        path: __dirname + '/build/client/dist',
        filename: '[name].js'
    }
};
