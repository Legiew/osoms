# OSOMS - Open Source Operation Management Software

## Development Setup

### Setup

- NodeJs and NPM are needed
- Recommended Editor: Visual Studio Code
- Recommended Editor Plugins
  - use 'Show Workspace Recommended Extensions' Command
  - or see /.vscode/extensions.json

### Installation

- Checkout out repository
- npm install
- npm run build

### Test

- npm test

### Run

- npm start
