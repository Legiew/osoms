import { Component } from '@angular/core';

import { OperationService } from './operation.service';

@Component({
  providers: [OperationService],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public operations = [];

  constructor(
    private operationService: OperationService
  ) {
    this.operationService.getOperations()
      .subscribe((operations) => {
        console.log('operations: ' + operations);
        this.operations = operations;
      },
      (err) => { console.log(err); });
  }

}
