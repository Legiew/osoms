import { expect } from 'chai';
import hello from '../src/hello';

describe('hello', () => {
    it('should return hello osoms', () => {
        const result = hello();

        expect(result).to.be.equal('Hello OSOMS!');
    });
});
