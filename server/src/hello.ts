/**
 * Hello OSOMS Function
 * @returns 'Hello OSOMS!'
 */
export default function hello(): string {
    return 'Hello OSOMS!';
}
