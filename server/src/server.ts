import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as http from 'http';
import * as mongoose from 'mongoose';
import * as path from 'path';

import { getApiRouter } from './router/api';

// Substituting mongoose's promise library (hack because type script wouldn't let us)
(mongoose as any).Promise = global.Promise;

mongoose.connect('mongodb://localhost/osoms');

const app = express();

app.use(express.static(path.join(__dirname, '../../client/dist')));
app.use(bodyParser.json());
app.use('/api', getApiRouter());

const server = http.createServer(app);

server.listen(30000, () => {
    console.log('Server is listening at http://127.0.0.1:30000');
});
