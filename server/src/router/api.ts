import * as express from 'express';

import { getEntryModel, IEntryModel } from '../schemata/entry';
import { getOperationModel } from '../schemata/operation';
import { getUnitModel } from '../schemata/unit';

export function getApiRouter(): express.Router {
    const router = express.Router();

    router.get('/entry', getEntries);
    router.post('/entry', addEntry);

    router.get('/operation', getOperations);
    router.get('/operation/:id', getOperationById);
    router.get('/operation/:id/entries', getEntriesByOperation);
    router.post('/operation', addOperation);
    router.post('/operation/:id/entry', addOperationIdToBody, addEntry);

    router.get('/unit', getUnits);
    router.get('/unit/:id', getUnitById);
    router.post('/unit', addUnit);

    return router;
}

const Entry = getEntryModel();
const Operation = getOperationModel();
const Unit = getUnitModel();

const getEntries = async (req: express.Request, res: express.Response) => {
    console.log('api - get entries');
    try {
        const entries = await Entry.find();
        res.send(entries);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const addEntry = async (req: express.Request, res: express.Response) => {
    console.log('api - add entry');
    try {
        const newEntry: IEntryModel  = new Entry();
        newEntry.timestamp = Date.now();
        newEntry.message = req.body.message;
        newEntry.createdAt = req.body.createdAt;
        newEntry.operation = req.body.operation;
        await newEntry.save();
        res.send(newEntry);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const getOperations = async (req: express.Request, res: express.Response) => {
    console.log('api - get operations');
    try {
        const operations = await Operation.find();
        res.send(operations);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const getOperationById = async (req: express.Request, res: express.Response) => {
    console.log('api - get operation by id');
    try {
        const operation = await Operation.findById(req.params.id);
        res.send(operation);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const addOperation = async (req: express.Request, res: express.Response) => {
    console.log('api - add operation');
    try {
        const newOperation = new Operation(req.body);
        await newOperation.save();
        res.send(newOperation);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const getUnits = async (req: express.Request, res: express.Response) => {
    console.log('api - get units');
    try {
        const units = await Unit.find();
        res.send(units);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const getUnitById = async (req: express.Request, res: express.Response) => {
    console.log('api - get unit by id');
    try {
        const unit = await Unit.findById(req.params.id);
        res.send(unit);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const addUnit = async (req: express.Request, res: express.Response) => {
    console.log('api - add unit');
    try {
        const newUnit = new Unit(req.body);
        await newUnit.save();
        res.send(newUnit);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const getEntriesByOperation = async (req: express.Request, res: express.Response) => {
    console.log('api - get entries by operation');
    try {
        const entries = await Entry.find({ operation: req.params.id });
        res.send(entries);
    } catch (error) {
        console.log(error);
        res.send(500, error);
    }
};

const addOperationIdToBody = (req: express.Request, res: express.Response, next: Function) => {
    console.log('api - add entry id to body');
    req.body.operation = req.params.id;
    next();
};
