import * as mongoose from 'mongoose';

interface ILocation {
    name: String;
    latitude: Number;
    longitude: Number;
    operation: String;
}

// tslint:disable-next-line:no-empty-interface
export interface ILocationModel extends ILocation, mongoose.Document { }

export function getLocationModel(): mongoose.Model<ILocationModel> {
    return mongoose.model<ILocationModel>('Entry', new mongoose.Schema({
        latitude: Number,
        longitude: Number,
        name: Number,
        operation: { type: mongoose.Schema.Types.ObjectId, ref: 'Operation' }
    }));
}
