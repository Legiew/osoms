import * as mongoose from 'mongoose';

interface IOperation {
    begin: Date;
    end: Date;
    name: String;
    location: String;
    description: String;
    finished: Boolean;
}

// tslint:disable-next-line:no-empty-interface
export interface IOperationModel extends IOperation, mongoose.Document { }

export function getOperationModel(): mongoose.Model<IOperationModel> {
    return mongoose.model<IOperationModel>('Operation', new mongoose.Schema({
        begin: Date,
        description: String,
        end: Date,
        finished: Boolean,
        location: String,
        name: String
    }));
}
