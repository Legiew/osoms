import * as mongoose from 'mongoose';

interface IEntry {
    message: String;
    createdAt: Number;
    timestamp: Number;
    operation: String;
}

// tslint:disable-next-line:no-empty-interface
export interface IEntryModel extends IEntry, mongoose.Document { }

export function getEntryModel(): mongoose.Model<IEntryModel> {
    return mongoose.model<IEntryModel>('Entry', new mongoose.Schema({
        createdAt: Number,
        message: String,
        operation: { type: mongoose.Schema.Types.ObjectId, ref: 'Operation', required: true },
        timestamp: Number // MongoDB verwalen lassen? (Mongoose)
    }));
}
