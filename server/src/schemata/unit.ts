import * as mongoose from 'mongoose';

interface IUnit {
    name: String;
    radioName2m: String;
    radioName4m: String;
    radioNameDigital: String;
    licensePlate: String;
    currentPosition: String;
    standardPosition: String;
    currentlyDeployed: Boolean;
    lastStatus: String;
    operation: String;
}

// tslint:disable-next-line:no-empty-interface
export interface IUnitModel extends IUnit, mongoose.Document { }

export function getUnitModel(): mongoose.Model<IUnitModel> {
    return mongoose.model<IUnitModel>('Unit', new mongoose.Schema({
        currentPosition: String,
        currentlyDeployed: Boolean,
        lastStatus: String,
        licensePlate: String,
        name: String,
        operation: { type: mongoose.Schema.Types.ObjectId, ref: 'Operation' },
        radioName2m: String,
        radioName4m: String,
        radioNameDigital: String,
        standardPosition: String
    }));
}
